INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (01, 'AC', 'Acre');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (02, 'AL', 'Alagoas');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (03, 'AM', 'Amazonas');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (04, 'AP', 'Amapá');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (05, 'BA', 'Bahia');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (06, 'CE', 'Ceará');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (07, 'DF', 'Distrito Federal');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (08, 'ES', 'Espírito Santo');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (09, 'GO', 'Goiás');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (10, 'MA', 'Maranhão');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (11, 'MG', 'Minas Gerais');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (12, 'MS', 'Mato Grosso do Sul');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (13, 'MT', 'Mato Grosso');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (14, 'PA', 'Pará');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (15, 'PB', 'Paraíba');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (16, 'PE', 'Pernambuco');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (17, 'PI', 'Piauí');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (18, 'PR', 'Paraná');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (19, 'RJ', 'Rio de Janeiro');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (20, 'RN', 'Rio Grande do Norte');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (21, 'RO', 'Rondônia');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (22, 'RR', 'Roraima');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (23, 'RS', 'Rio Grande do Sul');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (24, 'SC', 'Santa Catarina');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (25, 'SE', 'Sergipe');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (26, 'SP', 'São Paulo');
INSERT INTO `dbconsulta`.`state`
(`id`,
`name`,
`uf`) VALUES (27, 'TO', 'Tocantins');