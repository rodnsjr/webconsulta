(function() {

	/*Pacientes */
	var patientControllers = angular.module('patientControllers', ['ui.router']);
	
	patientControllers.config(function($stateProvider, $urlRouterProvider) {
		 
		  $urlRouterProvider.when('/create{pacienteId}', '/create{pacienteId}/personnal');
		  // Now set up the states
		  $stateProvider
		.state('pacientes.list', {
			url: "/list",
			templateUrl: "view/pacientes/list.html"
		})
		.state('pacientes.grid', {
			url: "/grid",
			templateUrl: "view/pacientes/grid.html"
		})
		.state('pacientes.table', {
			url: '',
			templateUrl: "view/pacientes/table.html"
		})
		.state('pacientes.create',{
			url: "/create?pacienteId",
			templateUrl: "view/pacientes/new.html",
			controller: function($state, $stateParams){
				//console.log($stateParams);
				if ($state.current.name == 'pacientes.create'){
					$state.transitionTo('pacientes.create.personnal', $stateParams);
				}
			}
		})
		.state('pacientes.create.personnal', {
			url:"/personnal",
			templateUrl: "view/pacientes/form/personnal.html",
			controller: "PatientCreateController"
		})
		.state('pacientes.create.contact', {
			url:"/contact",
			templateUrl: "view/pacientes/form/contact.html",
			controller: "PatientCreateController"
		})
		.state('pacientes.show', {
			url: "/show?pacienteId",
			templateUrl: "view/pacientes/paciente.html",
			controller: "PatientShowController"
		});
	});
	
	patientControllers.directive('eatClickIf', ['$parse', '$rootScope',
	                                             function($parse, $rootScope) {
	    return {
	      // this ensure eatClickIf be compiled before ngClick
	      priority: 100,
	      restrict: 'A',
	      compile: function($element, attr) {
	        var fn = $parse(attr.eatClickIf);
	        return {
	          pre: function link(scope, element) {
	            var eventName = 'click';
	            element.on(eventName, function(event) {
	              var callback = function() {
	                if (fn(scope, {$event: event})) {
	                  // prevents ng-click to be executed
	                  event.stopImmediatePropagation();
	                  // prevents href 
	                  event.preventDefault();
	                  return false;
	                }
	              };
	              if ($rootScope.$$phase) {
	                scope.$evalAsync(callback);
	              } else {
	                scope.$apply(callback);
	              }
	            });
	          },
	          post: function() {}
	        }
	      }
	    }
	  }
	]);
	
	currentPatient = {};
	
	patientControllers.controller("PatientShowController" , function($scope, $http, $state, $stateParams) {
		$scope.select = false;
		$scope.error = false;
		
		$scope.isChild = function(){
			if ($scope.patient){
				if ($scope.patient.birthDate){
					var novaString = $scope.patient.birthDate;
					var novaDateString = novaString.split('-');
					var novaDate = new Date(novaDateString[0], novaDateString[1], novaDateString[2]);
		
					var anoAtual = new Date().getFullYear();
					var anoNasc = novaDate.getFullYear();
					var idade = anoAtual - anoNasc;
		
					if (idade < 12){
						return true;
					}
				}
			}
			return false;
		}
		
		if ($stateParams.pacienteId){
			
			$http.get('rest/patient', { params: { patientId: $stateParams.pacienteId } } ).
			success(function(data) {
				//console.log(data);
				$scope.patient = data;
			})
			.error(function(data){
				//console.log('error');
				$scope.error = true;
			});
			
			
			//$scope.patient = pacientes[$stateParams.pacienteId];
			
			//console.log($scope.patient);
			
		}else{
			//console.log('no param');
			$scope.select = true;
		}
	});
	
	
	patientControllers.controller("PatientsShowController" , function($scope, $http) {
		
		//$scope.patients = pacientes;
		$scope.patients = {};
		
		$http.get('rest/people').
			success(function(data) {
				$scope.patients = data;
			});
		
		
		$scope.deletePatient = function(patient){
			delete patient.address.city;
			$http.post('rest/person/delete', angular.toJson(patient)).
			  success(function(data, status, headers, config) {
				  console.log('delete');
				  index = $scope.patients.indexOf(patient);
				  $scope.patients.splice(index,1);
			  }).
			  error(function(data, status, headers, config) {
				  //$scope.create = false;
			  });
		}
	});
		
	patientControllers.controller("PatientCreateController" , function($scope, $http, $state, $stateParams, $timeout) {
		
		$scope.currentPatient = currentPatient;
		if (!$scope.selectedState)
			$scope.selectedState = "Estado";
		if (!$scope.selectedCity)
			$scope.selectedCity = "Cidade";
		
		console.log($state.current.name);
		
		if ($state.current.name != 'pacientes.create.contact'){
			if ($stateParams.pacienteId){
				$http.get('rest/patient', { params: { patientId: $stateParams.pacienteId } } ).
				success(function(data) {
					//console.log('Editando o Paciente');
					//console.log(data);
					//Converte a date java p/ javascript
					if (data.birthDate){
						var novaString = data.birthDate;
						var novaDateString = novaString.split('-');
						var novaDate = new Date(novaDateString[0], novaDateString[1], novaDateString[2]);
						data['birthDate'] = novaDate;
					}
					
					if (data.address){
						if (data.address.city){
							$scope.selectedCity = data.address.city.name;
							$scope.selectedState = data.address.city.state.uf;
							$scope.currentCity = data.address.city;
							$scope.currentState = data.address.city.state;
							$scope.address = data.address;
						}
					}
					
					$scope.currentPatient = data;
					$scope.validPatient = true;
				})
				.error(function(data){
					//console.log('error');
					$scope.validPatient = false;
				});
			}else{
				$scope.validPatient = true;
			}
		}
		
		$scope.isFormPersonnalValid = function(){
			var isValid = $('.ui.form.personnal').form('validate form');
			
			return isValid;
		}
		
		$scope.isFormContactValid = function(){
			var isValid = $('.ui.form.contact').form('validate form');
			
			return isValid;
		}
		
		$scope.nextScreen = function(){
			if ($state.current.name == 'pacientes.create.personnal'){
				if ($scope.validPatient){
					currentPatient = $scope.currentPatient;
					
				}
				$state.go('pacientes.create.contact', $stateParams);
			}
		}
		
		$scope.register = function(){
			if ($state.current.name == 'pacientes.create.contact'){
				console.log('uat?');
			}
			if ($scope.address)
				if ($scope.address.number)
					$scope.address.number = parseInt($scope.address.number);
			var context = $scope.currentPatient;
			if ($scope.address)
				context['address'] = $scope.address;
			if ($scope.currentCity)
				context.address['city'] = '/rest/city/' + $scope.currentCity.id;
			
			console.log('salvando');
			console.log(context);
			
			$http.post('rest/person/create', angular.toJson(context)).
			  success(function(data, status, headers, config) {
				  //console.log('paciente criado');
				  $scope.create = true;
			  }).
			  error(function(data, status, headers, config) {
				  $scope.create = false;
			  });
		}
		
		$scope.selectState = function(state){
			//console.log(state);
			$scope.currentState = state;
			//console.log($scope.currentPatient);
			$http.get('rest/citys', { params: { state: state.id } } ).
			success(function(data) {
				console.log('retrieve!');
				$scope.citys = data;
			});
			
		}
		
		$scope.selectCity = function(city){
			$scope.currentCity = city;
		}
		
		$scope.selectGender = function(gender){
			console.log('selecting gender');
			if (!$scope.currentPatient.gender)
				$scope.currentPatient['gender'] = gender;
			else
				$scope.currentPatient.gender = gender;
			console.log(gender);
		}
		
		if ($scope.currentPatient.state){
			if ($scope.currentPatient.state.value >= 0){
				$scope.selectedState = $scope.currentPatient.state.name;
				//console.log($scope.selectedState);
			}
		}
		if ($scope.currentPatient.city){
			if ($scope.currentPatient.city.id >= 0){
				$scope.selectedCity = $scope.currentPatient.city.name;
				//console.log($scope.selectedState);
			}
		}
		
		$http.get('rest/states').
		success(function(data) {
			$scope.states = data;
		});
	});
	
	
	patientControllers.controller("ProntuarioController", function($scope, $element) {
		var registrationForm = $($element);

		$scope.user = {};

		$scope.isInvalid = function() {
			return !registrationForm.form('validate form');
		};

		$scope.register = function () {
			console.log('register');
			if (this.isInvalid()) {
				console.log('invalido');
				//return;
			}else{
				console.log('valido');
				console.log(this.user);
			}
		};
	});
	
	var homeControllers = angular.module('homeControllers', ['ui.router']);
	
	homeControllers.controller("HomeController", function($scope, $http){
		$http.get('rest/schedulingsToday').
		success(function(data) {
			$scope.agendamento = data;
		});
	});
		
	var consultasControllers = angular.module('consultasControllers', ['ui.router']);
	
	consultasControllers.config(function($stateProvider, $urlRouterProvider) {
		 
		  //
		  // Now set up the states
		  $stateProvider
		.state('agendamento.list', {
			url: "/list",
			templateUrl: "view/agendamento/list.html"
		})
		.state('agendamento.grid', {
			url: "/grid",
			templateUrl: "view/agendamento/grid.html"
		})
		.state('agendamento.table', {
			url: '/table',
			templateUrl: "view/agendamento/table.html"
		})
		.state('agendamento.create',{
			url: "/create?agendamentoId",
			templateUrl: "view/agendamento/form.html",
			controller: "AgendamentoCreateController"
		});
	});
	
	/* Agendamento/Consultas */
	consultasControllers.controller("AgendamentoShowController", function($scope, $http, $filter, $state){
		$scope.message = "Hoje";
		
		$scope.filteredDate = {};
		
		$scope.edit = function(item) {
			$scope.$broadcast('edit', item);
		};
		
		$scope.deletar = function(item){
			$http.post('rest/scheduling/delete', item).
			success(function(data, status, headers, config) {
				console.log('apagou');
				index = $scope.agendamento.indexOf(item);
				$scope.agendamento.splice(index,1);
			}).error(function(data, status, headers, config){
				console.log('erro');
			});
		}
		
		$scope.update = function(){
			console.log('novas')
			$http.get('rest/schedulings').
			success(function(data) {
				$scope.agendamento = data;
			});
		}
		
		$scope.filterDate = function(){
			console.log('filterDate');
			var date = $filter('date')(new Date($scope.filteredDate), "yyyy-MM-dd");
			console.log(date);
			$scope.message = date;
			$http.get('rest/schedulingsDate', { params: { date: date } } ).
			success(function(data) {
				$scope.agendamento = {};
				$scope.agendamento = data;
				console.log(data);
				//$scope.$apply();
				console.log($scope.message);
			});
		}
		
		$http.get('rest/schedulingsToday').
		success(function(data) {
			$scope.agendamento = data;
		});
		
		
	});
	
	consultasControllers.controller("AgendamentoCreateController", 
	        function($scope, $http, $state, $stateParams){
		
		$scope.currentClassification = "Selecionar Classificação";
		
		$scope.currentCovenant = "Selecionar Convênio";
		
		$scope.currentCategory = "Selecionar Categoria";
		
		$scope.categorias = [ ];
		
		$scope.covenants = [ {id: 1, name: "Unimed"}, {id:2, name:"IPE"}, {id:3, name: "SUS" }, {id: 4, name: "Particular" }];
		
		if ($stateParams.agendamentoId){
			//console.log($stateParams);
			//console.log($stateParams.agendamentoId);
			$http.get('rest/scheduling/id', { params: { agendamento: $stateParams.agendamentoId } } ).
			success(function(data) {
				//console.log(data);
				var novaString = data.date;
				var novaDateString = novaString.split('-');
				var novaDate = new Date(novaDateString[0], novaDateString[1], novaDateString[2]);
				data['date'] = novaDate;
				$scope.novoAgendamento = data;
				if ($scope.novoAgendamento.classification){
					$scope.currentClassification = classifications[$scope.novoAgendamento.classification];
				}if ($scope.novoAgendamento.covenant){
					$scope.currentCovenant = covenants[$scope.novoAgendamento.covenant];
					
					$scope.categorias = categories[$scope.novoAgendamento.covenant];
					
					if ($scope.novoAgendamento.category){
					//	console.log('categoria: ' + $scope.novoAgendamento.category);
					//	console.log($scope.categorias);
						for (index = 0; index < $scope.categorias.length; index++) {
						    var currentCat = $scope.categorias[index];
							if (currentCat['id'] == $scope.novoAgendamento.category){
								$scope.currentCategory = currentCat['name'];
								//console.log($scope.currentCategory);
							}
						}
					}
				}
			})
			.error(function(data){
				console.log('erro de servidor');
			});
		}else{
			$scope.novoAgendamento = {};			
		}
		
		$scope.selectCovenant = function(covenant){
			//console.log('selecting covenant');
			//console.log(covenant);
			$scope.novoAgendamento['covenant'] = covenant.id;
			//console.log($scope.novoAgendamento);
			$scope.categorias = categories[covenant.id];
			$scope.currentCategory = "Selecionar Categoria";
		}
		
		$scope.selectCategory = function(category){
			$scope.novoAgendamento['category'] = category.id;
		}
		
		$scope.register = function () {
			$scope.state = true;
			$http.post('rest/scheduling/create', $scope.novoAgendamento).
			  success(function(data, status, headers, config) {
				  //console.log(status);
				  $scope.state = false;
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
				  //console.log(status);
				  $scope.state = false;
			  });
			$scope.novoAgendamento = {};
			$state.go('agendamento.grid', $stateParams, { reload: true, inherit: false, notify: true });
		};
				
	});
	
	/*Model - Agendamento (static variable)*/
	/*
	var agendamento = [ { hour: "13:30",
						  date: "21, março, 2015",
						  patient: "Eduardo Casarin",
						  classification: 1,
						  phone: "3312-1000",
						  category: "Algo",
						  situation: "Algo",
						  covenant: 0,
						  observation: "Tudo bagual"
						}
						, 
						{
						  hour: "13:30",
						  date: "21, março, 2015",
						  patient: "Eduardo Casarin",
						  classification: 0,
						  phone: "3312-1000",
						  category: "Algo",
						  situation: "Algo",
						  covenant: 2,
						  observation: "Tudo bagual"
						} ];	
	*/
	
	/*Categorias ... ARRAY DOS INFERNO */
	var categories = [ ["nada"], [ {id: 1, name: "Amb - HOSP" }, 
	                     {id: 2, name: "Unimed Simples"} ], 
	                   [ { id: 3, name: "Categoria 1" }, 
	                     { id: 4, name: "Categoria 2" }  ],
	                   [ { id: 5, name: "SUS Simples" }, 
	                     { id: 6, name: "SUS Especial 1" }, 
	                     { id: 7, name: "SUS Especial 2" }] , 
	                   [ { id: 8, name: "Particular Simples" }, 
	                     { id: 9, name: "Particular VIP" } ] 
					  ];
	
	var unimed_categories = "";
	
	/*Classificações e Convênios*/
	var classifications = [ "Selecionar Classificação", "Eletiva", "Encaixe", "Emergência" ];
	
	var covenants = [ "Selecionar Convênio", "Unimed", "IPE", "SUS", "Particular" ];
	
	var pacientes = [ { id: 0, firstName: "João", lastName: "Mouser", gender: "Masculino", 
						birthDate: new Date(1940, 05, 28) , rg: "418757896", cpf: "134.021.812-74",
						phone:"55-3312-8490", celPhone: "55-9930-7580", email:"mouserJoao122@gmail.com", 
						observation: "Velho sedentário", civilState: "Casado",
						address: {
							street: "Rua Marechal Floriano", number: 757, neighborhood: "Centro", cep: "98801-650",
							complement: "Logo ali", city: {
								id: 12, name: "Santo Ângelo", uf: "SA", state: {
									id: 14, name: "Rio Grande do Sul", uf: "RS"
								}
							}
						},
						
						activities: [ 
						     { type: 0, date: new Date(2014, 08, 10), hour: "14:30", observation: "Dores de cabeça constantes" } ,
						     { type: 1, date: new Date(2015, 02, 14), hour: "13:50", observation: "Problemas nos rins" }
						]
						
						}, 
					
					{  id: 1, firstName: "Maria", lastName: "Belmont", gender: "Feminino", 
					   birthDate: new Date(1980 , 05 , 14 ), rg: "429434121", celPhone: "55-8412-7890", cpf: "869.838.583-44",
					   phone:"55-3314-5860", email:"BelmontVampireKiller@gmail.com", observation:"Destemida caçadora de vampiros",
					   civilState: "Solteira",
					   address: {
							street: "Rua Marechal Floriano", number: 757, neighborhood: "Centro", cep: "98801-650",
							complement: "Logo ali", city: {
								id: 12, name: "Santo Ângelo", uf: "SA", state: {
									id: 14, name: "Rio Grande do Sul", uf: "RS"
								}
							}
						}, 
						activities: [ 
								     { type: 1, date: new Date(2014, 08, 10), hour: "14:30", observation: "Dores de cabeça constantes" } ,
								     { type: 1, date: new Date(2015, 02, 14), hour: "13:50", observation: "Problemas nos rins" }
								]
						
						}				  
					];				  
					  
})();


