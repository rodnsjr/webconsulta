(function() {
	var app = angular.module('webConsultas', ['ngResource',
												'ui.router',
												'patientControllers',
												'consultasControllers',
												'homeControllers']);
	
	app.config(function($stateProvider, $urlRouterProvider) {
	  //
	  // For any unmatched url, redirect to /state1
	  $urlRouterProvider.otherwise("/home");
	  
	  //
	  // Now set up the states
	  $stateProvider
		.state('home', {
		  url: "/home",
		  templateUrl: "view/home.html",
		  controller: "MainCtrl"
		})
		.state('googleSearch', {
			url: "/google?question",
			templateUrl: 'view/google.html',
			controller: "GoogleSearchCtrl"
		})
		.state('pacientes', {
			url: "/pacientes",
			templateUrl: "view/pacientes.html",
			controller: function($state){
				if ($state.current.name == 'pacientes'){
					$state.transitionTo('pacientes.grid');
				}
			}
		})
		.state('agendamento', {
			url: "/agendamento",
			templateUrl: "view/horarios.html",
			controller: function($state){
				if ($state.current.name == 'agendamento'){
					$state.transitionTo('agendamento.table');
				}
			}
		});
	});
	
	app.config(['$httpProvider', function($httpProvider) {
        	$httpProvider.defaults.useXDomain = true;
        	delete $httpProvider.defaults.headers.common['X-Requested-With'];
    	}
	]);
	
	app.controller("GoogleSearchCtrl", function($scope, $state, $stateParams,$sce){
		//console.log($stateParams.question);
		//$scope.googleSearch = $stateParams.question;
		//var googleFullSearch = "http://www.google.com/?q="+$scope.googleSearch+"&btnG=Search"
		//$scope.searchUrl = $sce.trustAsResourceUrl(googleFullSearch);
	});
	
	app.controller("MainCtrl", function($scope, $http){
		$scope.question = "";
		
		var person = {"firstName":"Joao","lastName":"Silva",
				"gender":"Masculino","rg":"808080",
				"cpf":"00","phone":"0","celPhone":"0",
				"address":{"street":"Rua","number":5,"neighborhood":"Bairro",
				"city":{"name":"Cidade",
				"state":{"name":"Rio Grande do Sul","uf":"RS"},
				"uf":"AE"},"cep":"Cep","complement":"Complemento"},
				"email":"maluco@gmail.com","observation":"poisé","situation":0};
		/*
		$http.post('rest/person/create', person).
		  success(function(data, status, headers, config) {
			  console.log('all good!');
			  $scope.create = true;
		  }).
		  error(function(data, status, headers, config) {
			  console.log('error');
			  $scope.create = false;
		  });*/
		
		/*
		$http({
		    url: 'rest/person/create',
		    dataType: 'json',
		    method: 'POST',
		    data: angular.toJson(person),
		    headers: {
		        "Content-Type": "application/json"
		    }
		    }).success(function(response){
		        $scope.response = response;
		    }).error(function(error){
		        $scope.error = error;
		});
		*/
	});
	
})();