package scheduling;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import testes.SemanticResult;

@EnableAutoConfiguration
@RestController
public class SchedulingController {
	
	class SearchResult{
		Collection<SemanticResult> results;
		public SearchResult(Collection<Scheduling> results){
			this.results = getSemanticResults(results);
		}
		
		public final Collection<SemanticResult> getResults(){
			return results;
		}
		
		private Collection<SemanticResult> getSemanticResults(Collection<Scheduling> results){
			System.out.println("getSemanticResults");
			List<SemanticResult> retorno = new ArrayList<>();
			for (Scheduling scheduling : results) {
				retorno.add(new SemanticResult(scheduling));
			}
			return retorno;
		}
	}
	
	private final SchedulingRepository schedulingRepository;

	@Autowired
	public SchedulingController(SchedulingRepository schedulingRepository) {
		this.schedulingRepository = schedulingRepository;
	}

	@RequestMapping("rest/schedulings")
	Collection<Scheduling> readScheduling() {
		System.out.println("All Schedulings!");
		List<Scheduling> schedulings = new ArrayList<>();
		for (Scheduling scheduling : schedulingRepository.findAll()) {
			schedulings.add(scheduling);
			System.out.println("scheduling " + scheduling);
		}
		return schedulings;
	}
	
	@RequestMapping("rest/schedulingsDate")
	Collection<Scheduling> readSchedulingDate(@RequestParam(value="date") String date) {
		System.out.println("All Schedulings From Date: " + date);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date myDate = format.parse(date);
			List<Scheduling> exit = schedulingRepository.findByDate(myDate);
			exit.sort((sch1, sch2) -> sch1.getHour().compareTo(sch2.getHour()));
			return exit;
		} catch (ParseException e) {
			return readSchedulingToday();
		}
	}
	
	@RequestMapping("rest/schedulingsToday")
	Collection<Scheduling> readSchedulingToday() {
		System.out.println("All Schedulings Today!");
		List<Scheduling> exit = schedulingRepository.findByDate(Date.from(Instant.now()));
		//Filtra pela hora
		exit.sort((sch1, sch2) -> sch1.getHour().compareTo(sch2.getHour()));
		
		return exit;
	}
	
	@RequestMapping("rest/scheduling")
	Collection<Scheduling> readByPatient(@RequestParam(value="patient") String patient) {
		System.out.println("Looking for Schedule of ->  " + patient);
		List<Scheduling> schedulings = new ArrayList<>();
		for (Scheduling scheduling : schedulingRepository.findByPatientIgnoreCaseContaining(patient)) {
			schedulings.add(scheduling);
			//System.out.println("scheduling " + scheduling);
		}
		System.out.println(schedulings);
		return schedulings;
	}
	
	@RequestMapping("rest/scheduling/search")
	SearchResult searchForPatients(@RequestParam(value="searchString") String search) {
		System.out.println("Looking for Schedule of -> busca rod " + search);
		List<Scheduling> schedulings = new ArrayList<>();
		
		for (Scheduling scheduling : schedulingRepository.findByPatientLike(search)) {
				schedulings.add(scheduling);
		}
		
		//System.out.println(schedulings);
		return new SearchResult(schedulings);
	}
	/*@RequestMapping("rest/scheduling")
	Scheduling readPatient() {
		return schedulingRepository.findByPatient("Matheus Munareto").get(0);
	}*/
	@RequestMapping("rest/scheduling/id")
	Scheduling readID(@RequestParam(value="agendamento") Long id) {
		System.out.println("looking for: " + id);
        return schedulingRepository.findById(id).get(0);
	}
	
	
	@RequestMapping(value = "rest/scheduling/create", method = RequestMethod.POST)
	public ResponseEntity<Scheduling> createOrUpdate(@RequestBody Scheduling schedule) {
		System.out.println("criando!");
		try{
			if (schedule != null){
				schedulingRepository.save(schedule);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<Scheduling>(schedule, HttpStatus.UNAUTHORIZED);
		}

	    // TODO: call persistence layer to update
	    return new ResponseEntity<Scheduling>(schedule, HttpStatus.OK);
	}
	
	@RequestMapping(value = "rest/scheduling/delete", method = RequestMethod.POST)
	public ResponseEntity<Scheduling> delete(@RequestBody Scheduling schedule) {
		System.out.println("apagando!");
		try{
			if (schedule != null){
				schedulingRepository.delete(schedule);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<Scheduling>(schedule, HttpStatus.UNAUTHORIZED);
		}

	    // TODO: call persistence layer to update
	    return new ResponseEntity<Scheduling>(schedule, HttpStatus.OK);
	}
}
