package scheduling;

import java.util.Date;
import java.util.List;





import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;



public interface SchedulingRepository extends CrudRepository<Scheduling, Long> {
	
	List<Scheduling> findByPatientIgnoreCaseContaining(String patient);
	
	List<Scheduling> findById(Long id);

	List<Scheduling> findByDate(Date date);
	
	@Query("select p from Scheduling p where p.patient like %:patient%")
	List<Scheduling> findByPatientLike(@Param("patient") String patient);
	



}
