package scheduling;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Model para controle de agendamentos médicos
 * @author Rodney
 *
 */
@Entity
public class Scheduling {
	
	public static final byte CLASS_ELETIVA = 1;
	public static final byte CLASS_ENCAIXE = 2;
	public static final byte CLASS_EMERGECIA = 3;
	
	public static final byte AGREE_UNIMED = 1;
	public static final byte AGREE_IPE = 2;
	public static final byte AGREE_SUS = 3;
	public static final byte AGREE_PARTICULAR = 4;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	//@Temporal(TemporalType.TIME)
	private String hour;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date date;
	private String patient;
	private byte covenant;
	private byte classification;
	private String phone;	
	private String category;
	private byte situation;
	private String observation;
	
	private Long patientId;
	
	protected Scheduling(){}
	
	public Scheduling(Long id, String hour, Date date, String patient, byte covenant,byte classification, String phone,
			String category, byte situation, String observation) {
		
		this.id = id;
		this.hour = hour;
		this.date = date;
		this.patient = patient;
		this.classification = classification;
		this.phone = phone;
		this.covenant = covenant;
		this.category = category;
		this.situation = situation;
		this.observation = observation;

	}
	
	public Scheduling(Long id, String hour, Date date, String patient, byte covenant,byte classification, String phone,
			String category, byte situation, String observation, Long patientId) {
		this(id, hour, date, patient, covenant, classification, phone, category, situation, observation);
		this.patientId = patientId;
	}
	
	
	@Override
    public String toString() {
        return "\nPaciente: " + getPatient() + "\nData, e Hora: " + getDate();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public byte getCovenant() {
		return covenant;
	}

	public void setCovenant(byte covenant) {
		this.covenant = covenant;
	}

	public byte getClassification() {
		return classification;
	}

	public void setClassification(byte classification) {
		this.classification = classification;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public byte getSituation() {
		return situation;
	}

	public void setSituation(byte situation) {
		this.situation = situation;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getSearchString() {
		return getPatient() + "\n" + getHour() + "\n" + getDate().toString() + "\n" + getPhone();
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}	

}
