package people;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class City {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@ManyToOne
	private State state;
	private String uf;
	/*
	public City(Long id, String name, State state, String uf) {
		super();
		this.id = id;
		this.name = name;
		this.state = state;
		this.uf = uf;
	}
	
	public City(Long id, String name, State state){
		super();
		this.id = id;
		this.name = name;
		this.state = state;
	}*/
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
}
