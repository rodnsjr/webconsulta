package people;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Person {
	@Id	
	@GeneratedValue
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date registryDate;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date updateDate;
	private String firstName;
	private String lastName;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date birthDate;
	private String gender;
	private String rg;
	private String cpf;
	private String phone;
	private String celPhone;
	
	@Embedded
	private Address address;
	@Column(unique=true)
	private String email;
	private String observation;
	byte situation;
	
	protected Person(){}
	/*
	public Person(Long id, Date registryDate, Date updateDate,
			String firstName, String lastName, Date birthDate, String gender, String rg,
			String cpf, String phone, Address address, String email,
			String observation, byte situation) {
		super();
		this.id = id;
		this.registryDate = registryDate;
		this.updateDate = updateDate;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.gender = gender;
		this.rg = rg;
		this.cpf = cpf;
		this.phone = phone;
		if (address != null)
			this.address = address;
		this.email = email;
		this.observation = observation;
		this.situation = situation;
	}*/
	
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public byte getSituation() {
		return situation;
	}
	public void setSituation(byte situation) {
		this.situation = situation;
	}

	public String getCelPhone() {
		return celPhone;
	}

	public void setCelPhone(String celPhone) {
		this.celPhone = celPhone;
	}
	
	
	public String toSearchString() {
		if (birthDate != null)
			return (firstName + " " + lastName + "\n" + cpf + "\n" + birthDate.toString());
		return (firstName + " " + lastName + "\n" + cpf);
	}
	
	public String toString(){
		return String.format("Nome: %s\nSobrenome: %s\nRG: %s\nCPF: %s\nObs: %s\n", 
				firstName, lastName, rg, cpf, observation);
	}
}
