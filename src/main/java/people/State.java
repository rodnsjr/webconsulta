package people;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class State {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	/*@OneToMany(mappedBy="state")
	private Collection<City> city;*/
	private String uf;
	
	protected State(){}
	/*
	public State(Long id, String name, String uf) {
		super();
		this.id = id;
		this.name = name;
		//this.city = city;
		this.uf = uf;
	}*/
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/*
	public Collection<City> getCity() {
		return city;
	}
	public void setCity(Collection<City> city) {
		this.city = city;
	}*/
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	
}
