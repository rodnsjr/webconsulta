package people;

import javax.persistence.*;
@Embeddable
public class Address {

	private String street;
	@Column(nullable=true)
	private int number;
	private String neighborhood;
	@ManyToOne
	private City city;
	
	private String cep;
	private String complement;
	/*
	public Address(String street, int number, String neighborhood, City city,
			String cep, String complement) {
		super();
		this.street = street;
		this.number = number;
		this.neighborhood = neighborhood;
		this.city = city;
		this.cep = cep;
		this.complement = complement;
	}
	
	public Address(String street, int number, String neighborhood,
			String cep, String complement) {
		super();
		this.street = street;
		this.number = number;
		this.neighborhood = neighborhood;
		this.city = city;
		this.cep = cep;
		this.complement = complement;
	}*/

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getNeighborhood() {
		return neighborhood;
	}
	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	
	public String toString(){
		return String.format("Rua: %s \nNumber: %d\nBairro: %s\nComplemento: %s\n", 
				street, number, neighborhood, complement);
	}
}
