package people;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import testes.SemanticResult;


@EnableAutoConfiguration
@RestController
public class PersonController {
	
	class SearchResult{
		Collection<SemanticResult> results;
		public SearchResult(Collection<Person> results){
			this.results = getSemanticResults(results);
		}
		
		public final Collection<SemanticResult> getResults(){
			return results;
		}
		
		private Collection<SemanticResult> getSemanticResults(Collection<Person> results){
			System.out.println("getSemanticResults");
			List<SemanticResult> retorno = new ArrayList<>();
			for (Person person : results) {
				retorno.add(SemanticResult.getForPerson(person));
			}
			return retorno;
		}
	}
	
	private final PersonRepository peopleRepository;
	private final StateRepository stateRepository;
	private final CityRepository cityRepository;
	@Autowired
	public PersonController(PersonRepository peopleRepository,
			StateRepository stateRepository, CityRepository cityRepository) {
		this.peopleRepository = peopleRepository;
		this.stateRepository = stateRepository;
		this.cityRepository = cityRepository;
	}
	/*
	@Autowired
	public PersonController(PersonRepository peopleRepository) {
		super();
		this.peopleRepository = peopleRepository;
	}
	*/
	
	@RequestMapping("rest/states")
	public ResponseEntity<Collection<State>> findAllStates(){
		Collection<State> states = new Vector<>();
		try{
			for (State state : stateRepository.findAll()) {
				states.add(state);
			}
		}catch(Exception e){
			return new ResponseEntity<Collection<State>>(HttpStatus.UNAUTHORIZED);
		}
		 return new ResponseEntity<Collection<State>>(states,HttpStatus.OK);
	}
	
	@RequestMapping("rest/citys")
	public ResponseEntity<Collection<City>> findAllCities(@RequestParam(value="state") Long search){
		Collection<City> citys = new Vector<>();
		try{
			State state = stateRepository.findOne(search);
			citys = cityRepository.findByState(state);
		}catch(Exception e){
			return new ResponseEntity<Collection<City>>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<Collection<City>>(citys, HttpStatus.OK);
	}
	
	@RequestMapping("rest/people")
	public ResponseEntity<Collection<Person>> findAll(){
		Collection<Person> people = new Vector<>();
		try{
			for (Person person : peopleRepository.findAll()) {
				people.add(person);
			}
		}catch(Exception e){
			return new ResponseEntity<Collection<Person>>(HttpStatus.UNAUTHORIZED);
		}
		 return new ResponseEntity<Collection<Person>>(people,HttpStatus.OK);
	}
	
	@RequestMapping("rest/patient")
	public Person find(@RequestParam(value="patientId", defaultValue="0")Long id){
		System.out.println("procurando: " + id);
		Person p;
		
		p = peopleRepository.findOne(id);
		
		return p;
	}
	
	@RequestMapping(value= "rest/person/create", method = RequestMethod.POST)
	public ResponseEntity<Person> createOrUpdate(@RequestBody Person person){
		try{
			if (person != null){			
				if (person.getCelPhone().equals("(000) 0000-0000")){
					person.setCelPhone(null);
				}
				
				if (person.getPhone().equals("(000) 0000-0000")){
					person.setPhone(null);
				}
				System.out.println("Pessoa: " + person);
				System.out.println("Endereço: " + person.getAddress());
				
				peopleRepository.save(person);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<Person>(HttpStatus.UNAUTHORIZED);
		}

	    return new ResponseEntity<Person>(HttpStatus.OK);
	}
	
	@RequestMapping(value= "rest/person/delete", method = RequestMethod.POST)
	public ResponseEntity<Person> delete(@RequestBody Person person){
		try{
			if (person != null){			
				peopleRepository.delete(person);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<Person>(HttpStatus.UNAUTHORIZED);
		}

	    return new ResponseEntity<Person>(HttpStatus.OK);
	}
	
	@RequestMapping("rest/people/search")
	SearchResult searchForPatients(@RequestParam(value="searchString") String search) {
		
		Collection<Person> allpersons = new ArrayList<>();
		
		for (Person person : peopleRepository.findAll()) {
			if (person.toSearchString().contains(search)){
			
				allpersons.add(person);
			
			}
		}
		
		
		//System.out.println(schedulings);
		return new SearchResult(allpersons);
	}
}
