package testes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration
@ComponentScan(basePackages={"people","scheduling"})
@EnableAutoConfiguration
public class WebConsultasApplication {
	
    public static void main(String[] args) {   	
        SpringApplication.run(WebConsultasApplication.class, args);
         
    }
}
