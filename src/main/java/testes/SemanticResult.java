package testes;

import people.Person;
import scheduling.Scheduling;

public class SemanticResult {

	String title;
	String description;
	String url;
	
	private SemanticResult(){}
	
	public	SemanticResult(Scheduling schedule){
		title = schedule.getPatient()+"  ";
		description = schedule.getDate() + ", " + schedule.getHour();
		this.url = "#/agendamento/create?agendamentoId="+schedule.getId();
	}
	public String getTitle() {
		return title;
	}
	public String getDescription() {
		return description;
	}
	
	public String getUrl(){
		return url;
	}
	
	public static final SemanticResult getForPerson(Person p){
		SemanticResult sr = new SemanticResult();
		sr.title = p.getFirstName() + " " + p.getLastName();
		if (p.getBirthDate() != null)
			sr.description = "CPF: " + p.getCpf() + ", Data de Nascimento: " + p.getBirthDate();
		else
			sr.description = "CPF: " + p.getCpf();
		sr.url = "#/pacientes/show?pacienteId=" + p.getId();
		return sr;
	}

}
